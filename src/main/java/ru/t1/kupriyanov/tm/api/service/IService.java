package ru.t1.kupriyanov.tm.api.service;

import ru.t1.kupriyanov.tm.api.repository.IRepository;
import ru.t1.kupriyanov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}
