package ru.t1.kupriyanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @Nullable
    M add(@NotNull String userId, @NotNull M model);

    @Nullable
    List<M> findAll(@NotNull String userId);

    @Nullable
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    M removeOne(@NotNull String userId, @NotNull M model);

    @Nullable
    M removeOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    M removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeAll(@NotNull String userId);

    int getSize(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

}
