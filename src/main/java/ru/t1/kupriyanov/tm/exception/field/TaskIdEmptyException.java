package ru.t1.kupriyanov.tm.exception.field;

public final class TaskIdEmptyException extends AbstractFiledException {

    public TaskIdEmptyException() {
        super("Error! Task Id is empty...");
    }

}
